package local.myagodin.sltest;

import local.myagodin.sltest.impl.ConsoleLogger;
import local.myagodin.sltest.impl.CsvParser;

import java.io.File;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Main {

    private static final int MAX_WORKER_THREADS = 8;
    private static final int MAX_WAIT_TIMEOUT = 100;

    private static Map<LocalDateTime, Integer> result = new TreeMap<>();

    public static void main(String[] args) {
        Collection<File> files = new LinkedList<>();
        Arrays.stream(args).forEach(arg -> files.add(new File(arg)));

        processFiles(files);

        Map<LocalDateTime, Integer> groupedResult = result.entrySet().stream().collect(
                Collectors.groupingBy(
                        es -> es.getKey()
                                .truncatedTo(ChronoUnit.HOURS)
                                .withMinute(59 /* last minute in hour*/)
                                .withSecond(59 /* last second in minute */),
                        Collectors.summingInt(Map.Entry::getValue))
        );

        result.putAll(groupedResult);

        writeResult(result);
    }

    private static void processFiles(Collection<File> files) {
        Logger logger = new ConsoleLogger();
        Parser parser = CsvParser.getInstance();
        Processor processor = new Processor();
        processor.setParser(parser);

        ExecutorService es = Executors.newFixedThreadPool(Math.min(files.size(), MAX_WORKER_THREADS));
        files.forEach(
                file -> es.execute(new ProcessorTask(file, logger, processor, result))
        );
        es.shutdown();

        while (true) {
            try {
                if (es.awaitTermination(MAX_WAIT_TIMEOUT, TimeUnit.MILLISECONDS)) {
                    break;
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private static void writeResult(Map<LocalDateTime, Integer> data) {
        try (PrintWriter pw = new PrintWriter(System.out)) {
            data.entrySet().forEach(
                    es -> pw.println(es.getValue() + " ошибок за " + es.getKey())
            );
        }
    }
}
