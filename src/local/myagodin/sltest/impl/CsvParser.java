package local.myagodin.sltest.impl;

import local.myagodin.sltest.LogItem;
import local.myagodin.sltest.Parser;
import local.myagodin.sltest.ParserException;
import local.myagodin.sltest.Severity;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

// thread-safe, singleton

public class CsvParser implements Parser {

    private static final Parser PARSER = new CsvParser();

    private CsvParser() {
    }

    public static Parser getInstance() {
        return PARSER;
    }

    @Override
    public LogItem parse(String logString) throws ParserException {
        if (logString == null) {
            throw new ParserException("Incorrect null value in logString parameter");
        }

        String[] splitted = logString.split(";");
        if (splitted.length != 3) {
            throw new ParserException("Incorrect number of columns in logString parameter");
        }

        String sDateTime = splitted[0];
        String sSeverity = splitted[1];
        String sMessage = splitted[2];

        LogItem result = createLogItem(sDateTime, sSeverity, sMessage);

        return result;
    }

    private static LogItem createLogItem(String sDateTime, String sSeverity, String sMessage) throws ParserException {
        final LocalDateTime dateTime;
        try {
            dateTime = LocalDateTime.parse(sDateTime);
        } catch (DateTimeParseException e) {
            throw new ParserException("Incorrect format of 'dateTime' column: " + sDateTime, e);
        }

        final Severity severity;
        try {
            severity = Severity.valueOf(sSeverity);
        } catch (IllegalArgumentException e) {
            throw new ParserException("Incorrect value of 'severity' column: " + sSeverity, e);
        }

        final String message; // Никаких проверок не придумал особенных. Так, для проформы...
        if (sMessage.isEmpty()) {
            throw new ParserException("Incorrect empty value of 'message' column: " + sMessage);
        }
        message = sMessage;

        return LogItem.of(dateTime, severity, message);
    }
}
