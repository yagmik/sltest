package local.myagodin.sltest.impl;

import local.myagodin.sltest.Logger;

import java.util.Arrays;

public class ConsoleLogger implements Logger {
    @Override
    public void error(String message) {
        System.err.println(message);
    }

    @Override
    public void error(Throwable cause) {
        error(cause.getMessage(), cause);
    }

    @Override
    public void error(String message, Throwable cause) {
        System.err.println(String.format("%s\n%s", message, Arrays.toString(cause.getStackTrace())));
    }
}
