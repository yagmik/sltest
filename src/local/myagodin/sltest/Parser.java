package local.myagodin.sltest;

public interface Parser {
    LogItem parse(String logString) throws ParserException;
}
