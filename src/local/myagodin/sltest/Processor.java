package local.myagodin.sltest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class Processor {

    private Parser parser;

    public void setParser(Parser parser) {
        this.parser = parser;
    }

    public Map<LocalDateTime, Integer> process(InputStream src) throws IOException, ProcessorException {
        assert parser != null;
        assert src != null;

        Map<LocalDateTime, Integer> result = new HashMap<>();

        try (Reader isr = new InputStreamReader(src)) {
            try (BufferedReader br = new BufferedReader(isr)) {
                collectItems(br, result);
            }
        }

        return result;
    }

    private void collectItems(BufferedReader src, Map<LocalDateTime, Integer> dst) throws IOException,
            ProcessorException {

        assert parser != null;
        assert src != null;
        assert dst != null;

        int lineNo = 1;
        String line;
        while ((line = src.readLine()) != null) {

            final LogItem logItem;
            try {
                logItem = parser.parse(line);
            } catch (ParserException e) {
                String message = String.format("%s at line %d", e.getMessage(), lineNo);
                throw new ProcessorException(message, lineNo, e);
            }

            if (logItem.getSeverity() != Severity.ERROR) {
                continue;
            }

            LocalDateTime ldt = logItem.getDateTime().truncatedTo(ChronoUnit.MINUTES);

            dst.put(ldt, dst.getOrDefault(ldt, 0) + 1);

            ++lineNo;
        }
    }
}
