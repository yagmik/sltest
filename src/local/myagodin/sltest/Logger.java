package local.myagodin.sltest;

public interface Logger {
    void error(String message);

    void error(Throwable cause);

    void error(String message, Throwable error);
}
