package local.myagodin.sltest;

import java.time.LocalDateTime;
import java.util.Objects;

// TODO use lombok?..

// immutable
public class LogItem {
    private final LocalDateTime dateTime;
    private final Severity severity;
    private final String message;

    public static LogItem of(LocalDateTime dateTime, Severity severity, String message) {
        return new LogItem(dateTime, severity, message);
    }

    private LogItem(LocalDateTime dateTime, Severity severity, String message) {
        this.dateTime = Objects.requireNonNull(dateTime);
        this.severity = Objects.requireNonNull(severity);
        this.message = Objects.requireNonNull(message);
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Severity getSeverity() {
        return severity;
    }

    public String getMessage() {
        return message;
    }
}
