package local.myagodin.sltest;

public class ProcessorException extends Exception {

    private final int lineNo;

    public ProcessorException(String message, int lineNo, Throwable cause) {
        super(message, cause);

        this.lineNo = lineNo;
    }

    public int getLineNo() {
        return lineNo;
    }
}
