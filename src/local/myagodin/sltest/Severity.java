package local.myagodin.sltest;

public enum Severity {
    INFO,
    WARNING,
    ERROR
}
