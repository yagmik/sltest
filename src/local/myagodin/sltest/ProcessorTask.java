package local.myagodin.sltest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

public class ProcessorTask implements Runnable {

    private final File inputFile;

    private final Logger logger;

    private final Processor processor;

    private final Map<LocalDateTime, Integer> resultContainer;

    public ProcessorTask(File inputFile, Logger logger, Processor processor,
                         Map<LocalDateTime, Integer> resultContainer) {
        this.inputFile = Objects.requireNonNull(inputFile);
        this.logger = Objects.requireNonNull(logger);
        this.processor = Objects.requireNonNull(processor);
        this.resultContainer = Objects.requireNonNull(resultContainer);
    }

    @Override
    public void run() {
        try {
            processFile(inputFile);
        } catch (Throwable t) {
            logger.error(t);
        }
    }

    private void processFile(File inputFile) {
        Map<LocalDateTime, Integer> extractedItems;

        try (FileInputStream fis = new FileInputStream(inputFile)) {
            extractedItems = processor.process(fis);
        } catch (IOException | ProcessorException e) {
            String message = String.format("Error during process file '%s': %s", inputFile, e.getMessage());
            logger.error(message, e);
            return;
        }

        mergeItems(extractedItems, resultContainer);
    }

    private static final String LOCK_OBJECT = "lock object";

    private void mergeItems(Map<LocalDateTime, Integer> from, Map<LocalDateTime, Integer> to) {
        synchronized (LOCK_OBJECT) {
            from.forEach(
                    (key, value) -> to.merge(key, value, (oldValue, newValue) -> oldValue + newValue)
            );
        }
    }
}
